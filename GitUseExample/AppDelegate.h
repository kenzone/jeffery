//
//  AppDelegate.h
//  GitUseExample
//
//  Created by Zack on 2/22/13.
//  Copyright (c) 2013 tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
